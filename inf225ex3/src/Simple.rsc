module Simple

start syntax Program = Decl* decls Expr expr;

syntax Decl
	= "fun" ID name "(" ID arg ")" "=" Expr e ";"
	;
	 
syntax Expr
	= ID var                 // variables
	| NUM num                // integers
	| ID name "(" Expr arg ")"     // function call
	| "(" Expr e ")"        // parentheses
	> left (Expr e1 "*" Expr e2 | Expr e1 "/" Expr e2)       // multiplication
	> left (Expr e1 "+" Expr e2 | Expr e1 "-" Expr e2)       // addition
	> left (Expr e1 "\>" Expr e2 | Expr e1 "\<" Expr e2 | Expr e1 "\>=" Expr e2 | Expr e1 "\<=" Expr e2 | Expr e1 "==" Expr e2)       // addition
	> "let" ID var "=" Expr e1 "in" Expr e2 "end" 
	| "if" cond Expr "then" e1 Expr "else" Expr e2 "end"
	;
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z_0-9];

// numbers
lexical NUM = [\-]? [0-9] !<< [0-9]+ !>> [0-9];

lexical LAYOUT = [\ \t\n\r\f]*;

layout SPC = [\ \t\n\r\f]*;

