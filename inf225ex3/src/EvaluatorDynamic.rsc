module EvaluatorDynamic
import Simple;
import String;
import ParseTree;
import IO;

@doc{The type of environments; maps from strings to values}
alias Env = map[str, Value];

@doc{Values. Integers and functions are values}
data Value
	= Int(int intValue)
	| Fun(str argName, Expr expr, map[str, Value] env)
	;


@doc{For debugging.}
public void printEnv(Env env) {
	for(k <- env) {public Value eval((Expr)`<Expr e1>==<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}
	
		if(Fun(argName, expr, fenv) := env[k])
			println("  <k>: Fun(<argName>, <unparse(expr)>, <fenv>)");
		else
			println("  <k>: <env[k]>");
	}
}

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment.}
public Env define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env) {
	envNew = env + (unparse(f) : Fun(unparse(arg), body, env));
	println("Defining function <f>(<arg>) = <body>;");
	printEnv(env);
	// env[unparse(f)] = Fun(unparse(arg), body, env);
	return envNew;
}

@doc{Evaluate a program.}
public Value eval((Program)`<Decl* decls><Expr e>`) {
	Env env = ();
	for(Decl d <- decls) {  // declare functions first
		env = define(d, env);
	}
		
	return eval(e, env);
}

@doc{The value of an integer literal is the value of integer itself.}
public Value eval((Expr)`<NUM i>`, Env env) {
	return Int(toInt(unparse(i)));
}

@doc{The value of a variable is the value it's bound to in the environment.}
public Value eval((Expr)`<ID x>`, Env env) {
	return env[unparse(x)];
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in an environment where the
argument is bound.}
public Value eval((Expr)`<ID f>(<Expr e>)`, Env env) {
	fun = env[unparse(f)];	
	arg = eval(e, env);
	
	if(Fun(argName, body, declaredEnv) := fun) {
		funEnv = env; // dynamic scoping
		funEnv = funEnv + (unparse(f) : Fun(argName, body, funEnv));
		funEnv = funEnv + (argName : arg);
		return eval(body, funEnv);
	}
	else {
		throw "Expression should be a function";
	}
}

@doc{Parenthesis.}
public Value eval((Expr)`(<Expr e>)`, Env env) {
	return eval(e, env);
}
@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1> == <Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\<<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\><Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue > eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\<=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue <= eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\>=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue >= eval(e2, env).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public Value eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public Value eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	
	innerEnv = env + (unparse(x) : v);
	
	return eval(e2, innerEnv);
}

