module TestBasic
import Simple;
import EvaluatorDynamic;
import ParseTree;

test bool Numbers() { return eval((Program)`5`) == Int(5); }

test bool Arith1() { return eval((Program)`5+5`) == Int(10); }
test bool Arith2() { return eval((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return eval((Program)`10-2`) == Int(8); }
test bool Arith4() { return eval((Program)`10/2`) == Int(5); }
test bool Arith5() { return eval((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return eval((Program)`((10)/(2))`) == Int(5); }

test bool If1() { return eval((Program)`if 2*3 == 6 then 42 else 69 end`) == Int(42); }
test bool If2() { return eval((Program)`if 6 == 5 then 42 else 69 end`) == Int(69); }
test bool If3() { return eval((Program)`if 10 \< 5 then 1 else 0 end`) == Int(0); }

test bool OperatorPriority01() { return eval((Program)`1-2-3`) == eval((Program)`(1-2)-3`); }
test bool OperatorPriority02() { return eval((Program)`1/2/3`) == eval((Program)`(1/2)/3`); }
test bool OperatorPriority03() { return eval((Program)`1+2*3`) == eval((Program)`1+(2*3)`); }
test bool OperatorPriority04() { return eval((Program)`1+2/3`) == eval((Program)`1+(2/3)`); }
test bool OperatorPriority05() { return eval((Program)`1-2*3`) == eval((Program)`1-(2*3)`); }
test bool OperatorPriority06() { return eval((Program)`1-2/3`) == eval((Program)`1-(2/3)`); }
test bool OperatorPriority07() { return eval((Program)`1\>2+3`) == eval((Program)`1\>(2+3)`); }
test bool OperatorPriority08() { return eval((Program)`1*4\<2+3`) == eval((Program)`(1*4)\<(2+3)`); }
test bool OperatorPriority09() { return eval((Program)`1*4\>=2+3`) == eval((Program)`(1*4)\>=(2+3)`); }
test bool OperatorPriority10() { return eval((Program)`1*4==2+3`) == eval((Program)`(1*4)==(2+3)`); }
test bool OperatorPriority11() { return eval((Program)`1+2*3`) == eval((Program)`1+(2*3)`); }
test bool OperatorPriority12() { return eval((Program)`1+2-3+4-5`) == eval((Program)`((((1+2)-3)+4)-5)`); }
test bool OperatorPriority13() { return eval((Program)`1/2*3/4*5`) == eval((Program)`((((1/2)*3)/4)*5)`); }
