module Simple
import Numbers;

start syntax Program = Expr;

syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| ID "(" Expr ")"     // function call
	| "(" Expr ")"        // parentheses
	> left (Expr "*" Expr | Expr "/" Expr )       // multiplication
	> left (Expr "+" Expr | Expr "-" Expr )       // addition
	> "let" ID "=" Expr "in" Expr "end" 
	| "if" Expr "then" Expr "else" Expr "end"
	;

lexical Expr2
	= ID                  // variables
	| NUM                 // integers
	| ID LAYOUT "(" LAYOUT Expr2 LAYOUT ")"     // function call
	| "(" LAYOUT Expr2 LAYOUT ")"        // parentheses
	> left (Expr2 LAYOUT "*" LAYOUT Expr2 | Expr2 LAYOUT "/" LAYOUT Expr2 )       // multiplication
	> left Expr2 LAYOUT "+" LAYOUT Expr2       // addition
	;
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z_0-9];

// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];

lexical LAYOUT = [\ \t\n\r\f]*;

layout SPC = [\ \t\n\r\f]*;

public str pretty(Program p) {
	if(Expr e := p)
		return pretty(e);
	else
		return "[unknown program: \"<e>\"]";
}

public default str pretty(Expr e) {
	return "[unknown expr: \"<e>\"]";
}


public str pretty((Expr)`<ID i>`) {
	return "<i>";
}

public str pretty((Expr)`<NUM n>`) {
	return "<n>";
}

public str pretty((Expr)`<Expr e1>+<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>*<Expr e2>`) {
	return "(<pretty(e1)>*<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>/<Expr e2>`) {
	return "(<pretty(e1)>/<pretty(e2)>)";
}
public str pretty((Expr)`(<Expr e>)`) {
	return "(<pretty(e)>)";
}

public str pretty((Expr)`<ID f>(<Expr e>)`) {
	return "<f>(<pretty(e)>)";
}


alias Value = int;
alias Env = map[str, Value];

public Value eval((Expr)`<NUM i>`, Env env) {
	return 5; // toInteger(unparse(i));
}

public Value eval((Expr)`<ID x>`, Env env) {
	return env[unparse(x)];
}


public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return eval(e1, env) * eval(e2, env);
}

public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return eval(e1, env) + eval(e2, env);
}

public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return eval(e1, env) - eval(e2, env);
}

public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return eval(e1, env) / eval(e2, env);
}

public Value eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env) != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


public Value eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	
	env[unparse(x)] = v;
	
	return eval(e2, env);
}

