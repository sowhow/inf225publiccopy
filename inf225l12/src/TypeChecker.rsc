module TypeChecker
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;

data Type
	= Int()
	| Str()
	| Fun(Type paramType, Type returnType)
	;


@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public tuple[Env, Store] define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env) {
	envNew = declare(f,  Fun(arg, body, env), env);
	println("Defining function <f>(<arg>) = <body>;");
	printEnv(env);
	return envNew;
}

@doc{Evaluate a program.}
public Value check((Program)`<Decl* decls><Expr e>`) {
	try {
		Store store = [];
		Env env = newEnv();
		for(Decl d <- decls) {
			<env, typ> = define(d, env, store);
		}
			
		<val, typ> = check(e, env, store);
		return val;
	}
	catch ex: {
		println(ex);
		return Int(0);
	}
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Expr, Type] check(expr:(Expr)`<NUM i>`, Env env) {
	return <expr, Int()>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Expr, Type] check(expr:(Expr)`<ID x>`, Env env) {
	typ = lookup(x, env);
	return <expr, typ>;
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public tuple[Expr, Type] check(expr:(Expr)`<ID f>(<Expr e>)`, Env env) {
	funTyp = lookup(f, env);	
	<e, argTyp> = check(e, env);
	
	if(Fun(paramTyp, returnTyp) := funTyp) {
		if(argTyp == paramTyp)
			return <expr, returnTyp>;
		else
			throw "Wrong argument type <argTyp> to function <f>(<paramTyp>) -\> <returnTyp>, at <expr@\loc>";
	}
	else {
		throw "Variable <f> is not a function, at <f@\loc>";
	}
}

@doc{Parenthesis.}
public tuple[Expr, Type] check((Expr)`(<Expr e>)`, Env env) {
	return check(e, env);
}

public tuple[Expr, Type] checkArithOp(Expr e1, Expr e2, Env env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(<Int(), Int()> == <typ1, typ2>) {
		return <expr, Int()>;
	}
	else {
		throw "Wrong operands \<<typ1>, <typ2>\> to operator, at <expr@\loc>";
	}
}
@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, Type] check((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return checkArithOp(e1, e2, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, Type] check((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return checkArithOp(e1, e2, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, Type] check((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return checkArithOp(e1, e2, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Expr, Type] check((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return checkArithOp(e1, e2, env);
}

public tuple[Expr, Type] checkCompOp(Expr e1, Expr e2, Env env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(typ1 == typ2) {
		return <expr, Int()>;
	}
	else {
		throw "Can\'t compare <typ1> and <typ2>, at <expr@\loc>";
	}
}


@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, Type] check((Expr)`<Expr e1> == <Expr e2>`, Env env) {
	return checkCompOp(e1, e2, env);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, Type] check((Expr)`<Expr e1>\<<Expr e2>`, Env env) {
	return checkCompOp(e1, e2, env);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, Type] check((Expr)`<Expr e1>\><Expr e2>`, Env env) {
	return checkCompOp(e1, e2, env);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, Type] check((Expr)`<Expr e1>\<=<Expr e2>`, Env env) {
	return checkCompOp(e1, e2, env);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Expr, Type] check((Expr)`<Expr e1>\>=<Expr e2>`, Env env) {
	return checkCompOp(e1, e2, env);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Expr, Type] check((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	<c, cTyp> = check(c, env);
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(c != Int()) { 
		throw "Condition must be an integer";
	}
	
	
	if(typ2 != typ1) {
		throw "If branches must have same type";
	}
	
	return <expr, typ1>;
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public tuple[Expr, Type] check(expr:(Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	<_, typ1> = check(e1, env);

	env = declare(x,  typ1, env);
	
	<_, typ2> = check(e2, env);
	
	return <expr, typ2>;
}

public tuple[Expr, Type] check((Expr)`<ID x>=<Expr e>`, Env env) {
	<v, typ> = check(e, env, store);
	l = lookup(x, env);
	
	store = put(l, v, store);
	
	return <v, typ>;	
}

public tuple[Expr, Type] check((Expr)`<Expr e1>;<Expr e2>`, Env env) {
	<_, typ> = check(e1, env, store);
	<v2, typ> = check(e2, env, store);
	
	return <v2, typ>;
}

