module Environment
import Simple;
import ParseTree;
import List;


@doc{A scope maps strings to ints.}
alias Scope = map[str, int];

@doc{An environment is a list (stack) of scopes.}
alias Env = list[Scope];

@doc{Look up an identifier in the environment.}
public int lookup(ID name, Env env) {
	for(sc <- env) {
		if(unparse(name) in sc)
			return sc[unparse(name)];
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Declare an identifier, giving it a int. It must not already have
been declared in the same scope.}
public Env declare(ID name, int val, Env env) {
	sc = env[0]; // get the current scope
	
	if(unparse(name) in sc) {
		throw "Variable \'<unparse(name)>\' already declared at <name@\loc>";
	}
	else {
		sc[unparse(name)] = val;
		env[0] = sc;
		return env;
	}
}

@doc{Update the int of a variable in the environment}
public Env assign(ID name, int val, Env env) {
	for(i <- index(env)) {
		sc = env[i];
		
		if(unparse(name) in sc) { // find the scope in which the variable is declared
			sc[unparse(name)] = val;
			env[i] = sc;
			return env;
		}
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Make a new scope in the environment}
public Env enterScope(Env env) {
	return [(), *env];
}

@doc{Discard the current scope from the environment}
public Env exitScope(Env env) {
	return env[1..];
}

@doc{Make a new environment}
public Env newEnv() {
	return [()];
}


@doc{For debugging.}
public void printEnv(Env env) {
	i = 0;
	for(sc <- env) {
		println("  Scope <i>:");
		i = i + 1;
		for(k <- sc) {	
			if(Fun(argName, expr, fenv) := sc[k])
				println("  <k>: Fun(<unparse(argName)>, <unparse(expr)>, <fenv>)");
			else
				println("  <k>: <sc[k]>");
		}
	}
}
