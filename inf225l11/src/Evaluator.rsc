module Evaluator
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public tuple[Env, Store] define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env, Store store) {
	envNew = declare(f,  Fun(arg, body, env), env);
	println("Defining function <f>(<arg>) = <body>;");
	printEnv(env);
	return envNew;
}

@doc{Evaluate a program.}
public Value eval((Program)`<Decl* decls><Expr e>`) {
	try {
		Store store = [];
		Env env = newEnv();
		for(Decl d <- decls) {
			<env, store> = define(d, env, store);
		}
			
		<val, store> = eval(e, env, store);
		return val;
	}
	catch ex: {
		println(ex);
		return Int(0);
	}
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Value, Store] eval((Expr)`<NUM i>`, Env env, Store store) {
	return <Int(toInt(unparse(i))), store>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Value, Store] eval((Expr)`<ID x>`, Env env, Store store) {
	l = lookup(x, env);
	return <get(l, store), store>;
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public tuple[Value, Store] eval((Expr)`<ID f>(<Expr e>)`, Env env, Store store) {
	fun = lookup(f, env);	
	<arg, store> = eval(e, env, store);
	
	if(Fun(argName, body) := fun) {
		Env funEnv = envIn;
		env = enterScope(env);
		<v, store> = eval(body, env, store);
		env = exitScope(env);  // no effect here, since funEnv is discarded anyway
		return <v, store>;
	}
	else {
		throw "Expression should be a function";
	}
}

@doc{Parenthesis.}
public tuple[Value, Store] eval((Expr)`(<Expr e>)`, Env env, Store store) {
	return eval(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>*<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>+<Expr e2>`, Env env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue + v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>-<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>/<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1> == <Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\><Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue > eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<=<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue <= eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\>=<Expr e2>`, Env env, Store store) {
	return Int(eval(e1, env).intValue >= eval(e2, env).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Value, Store] eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env, Store store) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public tuple[Value, Store] eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env, Store store) {
	<v, store> = eval(e1, env, store);
	<l, store> = allocate(store);
	store = put(l, v, store);
	innerEnv = enterScope(env);
	
	innerEnv = declare(x,  l, innerEnv);
	
	<v2, store> = eval(e2, innerEnv, store);
	
	env = exitScope(innerEnv); // no effect here, since funEnv is discarded anyway
	
	return <v2, store>;
}

public tuple[Value, Store] eval((Expr)`<ID x>=<Expr e>`, Env env, Store store) {
	<v, store> = eval(e, env, store);
	l = lookup(x, env);
	
	store = put(l, v, store);
	
	return <v, store>;	
}

public tuple[Value, Store] eval((Expr)`<Expr e1>;<Expr e2>`, Env env, Store store) {
	<_, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	
	return <v2, store>;
}

