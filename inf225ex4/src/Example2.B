// Encoding pairs with updatable fields as lambda functions.

// First, let's use the technique from Example1 to make
// triples of int -> int functions.
fun tripleOfFunctions(int -> int f, int -> int g, int -> int h) : int -> int -> int =
  fun int i => if i == 0 then f else if i == 1 then g else h end end;

// Then, we encode updateable pairs as triples of functions.
// The first function takes 0 or 1 and returns the value of that
// element of the pair.
// The second and third function takes a new value and stores it
// in the first or second element respectively.
fun mutablePair(int x, int y) : int -> int -> int =
  tripleOfFunctions(fun int i => if i then y else x end,
                    fun int i => x = i,
                    fun int i => y = i);

// getters just call the pair function with the appropriate
// field selector (0 or 1) 
fun getX(int -> int -> int p) : int = p(0)(0);
fun getY(int -> int -> int p) : int = p(0)(1);

// setters return a new pair
fun setX(int -> int -> int p, int x) : int = p(1)(x);
fun setY(int -> int -> int p, int y) : int = p(2)(y);

// a simple test
let p = mutablePair(17, 19) in
  setX(p, 3);
  setY(p, 6);
  getX(p)*getY(p)
end
