module Simple

start syntax Program = Decl* decls Expr expr;

syntax Decl
	= "fun" ID "(" ID ")" "=" Expr ";"
	;
	 
syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| ID "(" Expr ")"     // function call
	| "(" Expr ")"        // parentheses
	> left (Expr "*" Expr | Expr "/" Expr )       // multiplication
	> left (Expr "+" Expr | Expr "-" Expr )       // addition
	> left (Expr "\>" Expr | Expr "\<" Expr | Expr "\>=" Expr | Expr "\<=" Expr | Expr "==" Expr )       // addition
	> "let" ID "=" Expr "in" Expr "end" 
	| "if" Expr "then" Expr "else" Expr "end"
	;
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z_0-9];

// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];

lexical LAYOUT = [\ \t\n\r\f]*;

layout SPC = [\ \t\n\r\f]*;

