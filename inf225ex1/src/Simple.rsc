module Simple

start syntax Program = Expr;

syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| Expr "+" Expr       // addition
	| Expr "*" Expr       // multiplication
	| ID "(" Expr ")"     // function call
	| "(" Expr ")"        // parentheses
	;
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_]+ !>> [a-zA-Z_];

// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];
